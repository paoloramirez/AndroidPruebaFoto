package com.hdc.desarrollo03.takemymoney;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by desarrollo03 on 01/02/18.
 */

public interface UploadImageInterface {
    @Multipart
    @POST("public_pruebaimagen/image/index.php")
    Call<UploadObject> uploadFile(@Part MultipartBody.Part file, @Part("file") RequestBody name);
}
